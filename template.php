<?php
/**
 * Implements hook_js_alter().
 */
function eight_js_alter(&$js) {
  drupal_add_js(array('eight' => array('theme_path' => drupal_get_path('theme', 'eight'))), 'setting');
  drupal_add_js('https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js', 'external');
}
/**
* Override HTML for guidelines for a text format.
*
* @param $variables
*   An associative array containing:
*   - format: An object representing a text format.
*
* @ingroup themeable
*/
function eight_filter_guidelines($variables) {
 return '';
}

function eight_filter_tips_more_info() {
 return '';
}

function eight_field_widget_form_alter(&$element, &$form_state, $context) {
  if(isset($element['add_more'])) {
    foreach(element_children($element['add_more']) as $key) {
      $element['add_more'][$key]['#value'] = str_replace('Add ', '', $element['add_more'][$key]['#value']);
    }
  }
}

function eight_form_alter(&$form, &$form_state, $form_id) {

  // dpm($form);
  // kpr($form_state);

  if($form_id === 'page_node_form') {
    if(isset($form['#node']->field_content) && !empty($form['#node']->field_content)) {
      if (isset($form['#node']->field_paragraphs[LANGUAGE_NONE])) {
        $paragraphs = element_children($form['#node']->field_paragraphs[LANGUAGE_NONE]);
        if(!empty($paragraphs)) {
          foreach($paragraphs as $key) {
            $eid = $form['#node']->field_paragraphs[LANGUAGE_NONE][$key]['value'];
            $form['field_paragraphs'][LANGUAGE_NONE][$key]['#prefix'] = '<div class="entity-id" rel="entity_' . $eid . '">';
          }
        }
      }
    }

    // Live edit load templates
    // Optimization: use drupal_find_base_themes()
    // kpr(drupal_find_base_themes(list_themes(), variable_get('theme_default'), $used_keys = array()));

    $basetheme_path = drupal_get_path('theme', 'basetheme') . '/templates/paragraphs';
    $basetheme_files = file_scan_directory($basetheme_path, '/paragraphs-item/');
    $defaulttheme_path = drupal_get_path('theme', variable_get('theme_default')) . '/templates/paragraphs';
    $defaulttheme_files = file_scan_directory($defaulttheme_path, '/paragraphs-item/');
    $files = array_merge($basetheme_files, $defaulttheme_files);

    $paragraphs = array();
    $bundle = 'columns';
    if(function_exists('paragraphs_bundle_load')) {
      $bundles = paragraphs_bundle_load();
      // kpr($bundles);

      // Zie paragraphs.module, lijn 835 > LANGUAGES
      foreach($bundles as $bundle_type => $bundle) {
        $item = array();
        $entity = paragraphs_field_get_entity($item, $bundle_type, 'field_paragraphs');

        $entity_view = $entity->view();
        // $entity_view1 = entity_view($entity, 'paragraphs_item', 'full', LANGUAGE_NONE);
        // kpr($entity);
        // kpr($entity_view);
        // kpr($entity_view1);
        // kpr(entity_get_info('paragraphs_item'));
        // kpr($bundle_type);
        // kpr($bundle);
        // kpr(field_view_mode_settings('paragraphs_item', $bundle_type));

        $entity_type = 'paragraphs_item';
        $field_name = 'field_paragraph_image';

        $info = field_info_instance($entity_type, $field_name, $bundle_type);
        $style = $info['display']['default']['settings']['image_style'];

        $paragraphs[$bundle_type] = '';
        if(isset($style) && $style !== '') {
          $paragraphs[$bundle_type] .= '<span class="image-style" rel="' . $style . '"></span>';
          unset($style);
        }
        $paragraphs[$bundle_type] .= render($entity_view);
      }

    }

    // kpr($paragraphs);


    // foreach($files as $file) {
    //   $name = str_replace('.tpl', '', str_replace('-', '_', $file->name));
    //   $render_array = array('#theme' => $name);
    //   // kpr($render_array);
    //   $paragraphs[$name] = drupal_render($render_array);
    //   // if($name !== 'paragraphs_item' && $name !== 'paragraphs_items') {
    //   //   $paragraphs[$name] = theme_render_template($file->uri, array(
    //   //     'content' => array()
    //   //   ));
    //   // }
    // }
    // $paragraphs[$name] = drupal_render($render_array);
    // unset($paragraphs['paragraphs_item']);
    // unset($paragraphs['paragraphs_items']);

    $form['#attached']['js'][] = array(
      'data' => array(
        'paragraphs' => $paragraphs
      ),
      'type' => 'setting'
    );
  }

  // if($form_id === 'taxonomy_form_term') {
    // kpr($form);
    // if(isset($form['field_content']) && !empty($form['field_content'])) {
    //   $paragraphs = element_children($form['field_content'][LANGUAGE_NONE]);
    //   foreach($paragraphs as $key) {
    //     $form['field_content'][LANGUAGE_NONE][$key]['paragraph_bundle_title']['info']['#markup'] = str_replace('Paragraph type: ', '', $form['field_content'][LANGUAGE_NONE][$key]['paragraph_bundle_title']['info']['#markup']);
    //   }
    // }
  // }
}

function eight_theme() {
  $basetheme_path = drupal_get_path('theme', 'basetheme') . '/templates/paragraphs';
  $basetheme_files = file_scan_directory($basetheme_path, '/paragraphs-item/');
  $defaulttheme_path = drupal_get_path('theme', variable_get('theme_default')) . '/templates/paragraphs';
  $defaulttheme_files = file_scan_directory($defaulttheme_path, '/paragraphs-item/');
  $files = array_merge($basetheme_files, $defaulttheme_files);

  $templates = array();
  foreach($files as $key => $value) {
    $name = str_replace('.tpl', '', $value->name);
    $template = str_replace('-', '_', $name);
    $templates[$template] = array(
      'name' => $name,
      'template' => $name,
      'path' => str_replace('/' . $value->filename, '', $value->uri),
      'variables' => array(
        'content' => array()
      ),
      // 'render element' => 'element',
    );
  }

  return $templates;
}

function eight_process_field(&$variables, $hook) {
  // Todo: create user permission for using paragraphs
  // if(isset($variables['element']['#field_type']) && $variables['element']['#field_type'] === 'paragraphs' && !user_has_role(1)) {
  //   kpr("KAKA");
  //     $items = &$variables['items'];
  //     if(empty($items)) {
  //       $items[] = array(
  //         '#markup' => '',
  //       );
  //     }

  //     foreach($items as $key => $paragraph) {
  //       // kpr($paragraph);
  //       $item = &$variables['items'][$key]['entity']['paragraphs_item'];
  //       reset($item);
  //       // kpr($item[key($item)]);
  //       $bundle = (isset($item[key($item)]['bundle'])) ? $item[key($item)]['bundle'] : '';
  //       $item[key($item)]['#prefix'] = '<div id="entity_' . key($item) . '" class="entity-paragraphs-item ' . $bundle . '">';
  //       $item[key($item)]['#suffix'] = '</div>';
  //     }
  // }
}

function eight_node_view_alter(&$build) {
  $node = $build['#node'];
  if (!empty($node->nid)) {
    $build['#contextual_links']['node'] = array('node', array($node->nid));
  }
}

/**
 * Theme helper function. Print location of template file.
 */
if(!function_exists('theme_helper')) {
  function theme_helper($file) {
    if(_the_aim_get_environment() == THE_AIM_ENVIRONMENT_DEVELOP) {
      print "<!-- " . $file . "-->";
    }
  }
}