<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
// kpr($content);
$isEditing = false;
if(isset($_GET['mode']) && $_GET['mode'] === 'preview') {
  $isEditing = true;
}
?>
<?php if(isset($content) && count($content) > 0) { ?>
<?php if($isEditing) { ?>
<?php /* PHP VERSION SPECIFIC array_values
<div id="entity_<?php print array_values($content)[0]['#object']->item_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
*/?>
<?php } ?>
  <div class="content"<?php print $content_attributes; ?>>
    <?php print render($content); ?>
  </div>
<?php if($isEditing) { ?>
<?php /*
</div>
*/?>
<?php } ?>
<?php } ?>