<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

// kpr($content);
// kpr(get_defined_vars());
$isEditing = false;
if(isset($_GET['mode']) && $_GET['mode'] === 'preview' || stripos($_SERVER['REQUEST_URI'], '/edit') > 0) {
  $isEditing = true;
}
?>
<?php if(isset($content) && count($content) > 0) { ?>
<?php if($isEditing) { ?>
<div id="entity_<?php print array_values($content)[0]['#object']->item_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
<?php } ?>
  <div class="columns">
<?php if($isEditing) { ?>
    <div class="entity-content">
<?php } ?>
    <?php print render($content['field_paragraph_text']); ?>
<?php if($isEditing) { ?>
    </div>
<?php } ?>
  </div>
<?php if($isEditing) { ?>
</div>
<?php } ?>
<?php } else { ?>
<?php if($isEditing) { ?>
<div id="entity_[id]" class="entity entity-paragraphs-item paragraphs-item-columns">
<?php } ?>
  <div class="columns">
<?php if($isEditing) { ?>
    <div class="entity-content"></div>
<?php } ?>
  </div>
<?php if($isEditing) { ?>
</div>
<?php } ?>
<?php } ?>