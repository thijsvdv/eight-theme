(function($) {
  $(document).ready(function() {
    $('.description').siblings('label').wrap('<a href="javascript:;" class="description-toggle"></a>');

    $('body').on('mouseenter mouseover', '.field-multiple-table td', function(e) {
      e.stopPropagation();
      $('.hover').removeClass('hover');
      $(this).addClass('hover');
    });

    $('body').on('mouseleave', '.horizontal-tabs-panes', function(e) {
      $('.hover').removeClass('hover');
    });

    $('body').on('blur', '#edit-meta-title, #edit-title', function() {
      setTitle();
    });


    // $('.vertical-tab-button.selected').removeClass('selected');
    // $('.vertical-tab-button').eq(1).addClass('selected');

  });

  $(window).load(function() {
    $('body')
      .on('click', '.js-toggle-layout', function() {
        $(this).parent().next().find('.group-classes:first').toggleClass('is-open');
      })

    window.closedParagraphs = [];
    Drupal.behaviors.eight = {
      attach: function(context, settings) {
        setTitle();
        // console.log('Drupal.behaviors.eight.attach');
        // Click to collapse
          $('.field-multiple-drag').each(function() {
            $(this).once('add-buttons', function() {
              if($(this).next().children('.group-classes').length > 0 || $(this).next().children('.ajax-new-content').children('.group-classes').length > 0) {
                $(this).prepend('<a href="javascript:;" class="js-toggle-layout" title="Open layout-paneel"></a>');
              }
              // $(this).prepend('<a href="javascript:;" class="js-delete" title="Verwijder deze paragraaf"></a>');
              $(this).prepend($(this).next().find('.form-actions:last').removeClass('form-wrapper'));
            });
          });

          $('em.placeholder').parent().once('toggle-paragraphs', function() {

            $(this).click(function() {
              $(this).closest('td').toggleClass('collapsed');

              // openParagraphs.push($(this).parent().attr('rel'));
              closedParagraphs = [];
              $('.collapsed').each(function(item) {
                closedParagraphs.push($(this).find('.entity-id').attr('rel'));
              });
            });
          });

          // Collapse all/previously closed paragraphs
            if(closedParagraphs.length === 0) {
              $('#edit-field-paragraphs .field-multiple-table .field-multiple-drag + td').addClass('collapsed');
            } else {
              closedParagraphs.forEach(function(id) {
                $('.entity-id[rel="' + id + '"]').parent().addClass('collapsed');
              });
            }
      }
    }
    Drupal.behaviors.eight.attach();
  });

  function setTitle() {
    if($('.field-name-field-paragraph-info-title').length > 0) {
      var title, desc;
      if($('#edit-meta-title').length > 0 && $('#edit-meta-title').val() !== '') {
        title = $('#edit-meta-title').val();
        desc = '<a href="javascript:;" class="description-toggle"><div class="description">Dit is de <a href="#edit-meta-title">Commercial title</a>. Om aan te passen: klik onderaan de SEO tab open.</div>';
      } else {
        title = $('.form-item-title input').val() || $('#edit-title').val();
        desc = '<a href="javascript:;" class="description-toggle">&nbsp;</a><div class="description">Dit is de <a href="#edit-title">Title</a>. Om aan te passen: pas bovenaan het title veld aan.</div>';
      }
      $('.field-name-field-paragraph-info-title').html('<h1>' + title + '</h1>' + desc);
    }
  }




})(jQuery);